/*
  A program which opens a window and draws three arms in a hierarchy

  Use the arrow keys and PgUp,PgDn, 
  keys to make the arms move.

  Use the keys 1,2 and 3 to switch between arms.

  Written by - 
               Harshavardhan Kode
*/
#ifndef _MAIN_HPP_
#define _MAIN_HPP_

// Defining the ESCAPE Key Code
#define ESCAPE 27
// Defining the DELETE Key Code
#define DELETE 127

#include <vector>
#include <fstream>
#include "gl_framework.hpp"
#include "shader_util.hpp"
#include "glm/vec3.hpp"
#include "glm/vec4.hpp"
#include "glm/mat4x4.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "hierarchy_node.hpp"
#include "texture.hpp"
#include "frame.hpp"

typedef enum {
    PRIM_FRUST_1,
    PRIM_CUBE,
    PRIM_CYL,
    PRIM_CFRUST_1,
    PRIM_CFRUST_2,
    PRIM_HSPH,
    PRIM_HRHSPH,
    PRIM_SQ,
    PRIM_NONE
} primType;


// This is the "main class", contains most state and overall driving logic
class MainClass {
    csX75::HNode* makeNode(csX75::HNode* myParent, primType myType, glm::vec3 myTrans, float myXrot, float myYrot, float myZrot, glm::vec3 myScale, char* myTex, int myTexResX, int myTexResY, GLfloat myShininess, int movement_mask, MovableParams *params);
    void initModel();
    void initBuffersGL(void);
    void renderGL();

    std::vector<std::vector<glm::vec4> > primitives, primCols, primNorms;
    std::vector<std::vector<glm::vec2> > primTexs;

    GLuint tex, cubeTex, skyboxVAO, skyboxVBO;

    GLuint shaderProgram, shaderProgramSkbox;
    std::vector<csX75::HNode*> root_list;



    glm::mat4 rotation_matrix;
    glm::mat4 projection_matrix;
    glm::mat4 c_rotation_matrix;
    glm::mat4 lookat_matrix;

    glm::mat4 model_matrix;
    glm::mat4 view_matrix;

    glm::mat4 modelview_matrix;

    public:
    /*// Translation Parameters
    GLfloat xpos=0.0,ypos=0.0,zpos=0.0;
    // Rotation Parameters
    GLfloat xrot=0.0,yrot=0.0,zrot=0.0;*/
    // Camera position and rotation Parameters
    GLfloat c_xpos = 0.0, c_ypos = 0.0, c_zpos = 2.0;
    GLfloat c_up_x = 0.0, c_up_y = 1.0, c_up_z = 0.0;
    GLfloat c_xrot=0.0,c_yrot=0.0,c_zrot=0.0;

    //Enable/Disable perspective view
    bool enable_perspective=true;
    //Shader program attribs
    GLuint vPosition, vColor, vTex, vNorm, eye, ambComp, lights, lightColors, shiny, vSkboxPos, vSkboxProjView, lightEnabled, spot, spotDir, spotColor, spotEnabled;

    //global matrix stack for hierarchical modelling
    std::vector<glm::mat4> matrixStack;

    std::vector<frame> frames;

    csX75::HNode* curr_node;

    GLuint uModelMatrix, uViewMatrix;

    frame myFrame;
    ModifiableNodes nodes;
    ModifiableNodesR2 nodes2;


    int main(int argc, char** argv);
};


//-------------------------------------------------------------------------

#endif
