#include "gl_framework.hpp"
#include "hierarchy_node.hpp"
#include "frame.hpp"
#include "main.hpp"

extern MainClass mc;
int current_model = 0; // 0 = IG-88, 1 = R2-D2

namespace csX75
{
  //! Initialize GL State
  void initGL(void)
  {
    //Set framebuffer clear color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    //Set depth buffer furthest depth
    glClearDepth(1.0);
    //Set depth test to less-than
    glDepthFunc(GL_LESS);
    //Enable depth testing
    glEnable(GL_DEPTH_TEST);
  }
  
  //!GLFW Error Callback
  void error_callback(int error, const char* description)
  {
    std::cerr<<description<<std::endl;
  }
  
  //!GLFW framebuffer resize callback
  void framebuffer_size_callback(GLFWwindow* window, int width, int height)
  {
    //!Resize the viewport to fit the window size - draw to entire window
    glViewport(0, 0, width, height);
  }
  
  //!GLFW keyboard callback
  void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
  {
    //!Close the window if the ESC key was pressed
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
      glfwSetWindowShouldClose(window, GL_TRUE);
    else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.ry-=10;
    else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.ry+=10;
    else if (key == GLFW_KEY_UP && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.rx-=10;
    else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.rx+=10;
    else if (key == GLFW_KEY_COMMA && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.rz-=10;
    else if (key == GLFW_KEY_PERIOD && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.rz+=10;
    else if (key == GLFW_KEY_K && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.ty-=0.2;
    else if (key == GLFW_KEY_I && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.ty+=0.2;
    else if (key == GLFW_KEY_J && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.tx-=0.2;
    else if (key == GLFW_KEY_L && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.tx+=0.2;
    else if (key == GLFW_KEY_U && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.tz-=0.2;
    else if (key == GLFW_KEY_O && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.tz+=0.2;
    else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
      mc.myFrame.camera.ry-=10;
    else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
      mc.myFrame.camera.ry+=10;
    else if (key == GLFW_KEY_UP && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
      mc.myFrame.camera.rx-=10;
    else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
      mc.myFrame.camera.rx+=10;
    else if (key == GLFW_KEY_COMMA && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
      mc.myFrame.camera.rz-=10;
    else if (key == GLFW_KEY_PERIOD && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
      mc.myFrame.camera.rz+=10;
    else if (key == GLFW_KEY_K && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
      mc.myFrame.camera.ty-=0.2;
    else if (key == GLFW_KEY_I && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
      mc.myFrame.camera.ty+=0.2;
    else if (key == GLFW_KEY_J && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
      mc.myFrame.camera.tx-=0.2;
    else if (key == GLFW_KEY_L && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
      mc.myFrame.camera.tx+=0.2;
    else if (key == GLFW_KEY_U && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
      mc.myFrame.camera.tz-=0.2;
    else if (key == GLFW_KEY_O && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
      mc.myFrame.camera.tz+=0.2;
    else if (key == GLFW_KEY_K && action == GLFW_PRESS && mods & GLFW_MOD_CONTROL)
      mc.myFrame.light1pos.ty-=0.2;
    else if (key == GLFW_KEY_I && action == GLFW_PRESS && mods & GLFW_MOD_CONTROL)
      mc.myFrame.light1pos.ty+=0.2;
    else if (key == GLFW_KEY_J && action == GLFW_PRESS && mods & GLFW_MOD_CONTROL)
      mc.myFrame.light1pos.tx-=0.2;
    else if (key == GLFW_KEY_L && action == GLFW_PRESS && mods & GLFW_MOD_CONTROL)
      mc.myFrame.light1pos.tx+=0.2;
    else if (key == GLFW_KEY_U && action == GLFW_PRESS && mods & GLFW_MOD_CONTROL)
      mc.myFrame.light1pos.tz-=0.2;
    else if (key == GLFW_KEY_O && action == GLFW_PRESS && mods & GLFW_MOD_CONTROL)
      mc.myFrame.light1pos.tz+=0.2;
    else if (key == GLFW_KEY_K && action == GLFW_PRESS && mods & GLFW_MOD_ALT)
      mc.myFrame.light2pos.ty-=0.2;
    else if (key == GLFW_KEY_I && action == GLFW_PRESS && mods & GLFW_MOD_ALT)
      mc.myFrame.light2pos.ty+=0.2;
    else if (key == GLFW_KEY_J && action == GLFW_PRESS && mods & GLFW_MOD_ALT)
      mc.myFrame.light2pos.tx-=0.2;
    else if (key == GLFW_KEY_L && action == GLFW_PRESS && mods & GLFW_MOD_ALT)
      mc.myFrame.light2pos.tx+=0.2;
    else if (key == GLFW_KEY_U && action == GLFW_PRESS && mods & GLFW_MOD_ALT)
      mc.myFrame.light2pos.tz-=0.2;
    else if (key == GLFW_KEY_O && action == GLFW_PRESS && mods & GLFW_MOD_ALT)
      mc.myFrame.light2pos.tz+=0.2;
     else if (key == GLFW_KEY_V && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT && mods & GLFW_MOD_CONTROL)
      mc.myFrame.spotpos.enabled=!mc.myFrame.spotpos.enabled;   
     else if (key == GLFW_KEY_V && action == GLFW_PRESS && mods & GLFW_MOD_ALT)
      mc.myFrame.light2pos.enabled=!mc.myFrame.light2pos.enabled;   
     else if (key == GLFW_KEY_V && action == GLFW_PRESS && mods & GLFW_MOD_CONTROL)
      mc.myFrame.light1pos.enabled=!mc.myFrame.light1pos.enabled;  
    else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
      mc.curr_node->dec_ry();
    else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
      mc.curr_node->inc_ry();
    else if (key == GLFW_KEY_UP && action == GLFW_PRESS)
      mc.curr_node->dec_rx();
    else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
      mc.curr_node->inc_rx();
    else if (key == GLFW_KEY_COMMA && action == GLFW_PRESS)
      mc.curr_node->dec_rz();
    else if (key == GLFW_KEY_PERIOD && action == GLFW_PRESS)
      mc.curr_node->inc_rz();
    else if (key == GLFW_KEY_K && action == GLFW_PRESS)
      mc.curr_node->dec_ty();
    else if (key == GLFW_KEY_I && action == GLFW_PRESS)
      mc.curr_node->inc_ty();
    else if (key == GLFW_KEY_J && action == GLFW_PRESS)
      mc.curr_node->dec_tx();
    else if (key == GLFW_KEY_L && action == GLFW_PRESS)
      mc.curr_node->inc_tx();
    else if (key == GLFW_KEY_U && action == GLFW_PRESS)
      mc.curr_node->dec_tz();
    else if (key == GLFW_KEY_O && action == GLFW_PRESS)
      mc.curr_node->inc_tz();
    else if (key == GLFW_KEY_P && action == GLFW_PRESS)
      mc.enable_perspective = !mc.enable_perspective;   
    else if (key == GLFW_KEY_A  && action == GLFW_PRESS)
      mc.c_yrot -= 10.0;
    else if (key == GLFW_KEY_D  && action == GLFW_PRESS)
      mc.c_yrot += 10.0;
    else if (key == GLFW_KEY_W  && action == GLFW_PRESS)
      mc.c_xrot -= 10.0;
    else if (key == GLFW_KEY_S  && action == GLFW_PRESS && !(mods & GLFW_MOD_CONTROL))
      mc.c_xrot += 10.0;        
    else if (key == GLFW_KEY_Q  && action == GLFW_PRESS)
      mc.c_zrot -= 10.0;
    else if (key == GLFW_KEY_E  && action == GLFW_PRESS)
      mc.c_zrot += 10.0; 
    else if (key == GLFW_KEY_M && action == GLFW_PRESS) {
      if (current_model == 0) {
        current_model = 1;
        std::cout<<"Posing R2-D2"<<std::endl;
        mc.curr_node = mc.nodes2.body;
      } else {
        current_model = 0;
        std::cout<<"Posing IG-88"<<std::endl;
        mc.curr_node = mc.nodes.body;       
      }
    }
    else if (key == GLFW_KEY_S && action == GLFW_PRESS && mods & GLFW_MOD_CONTROL) {
        mc.myFrame.writeToFile();
    }

    if (current_model == 0) { // IG 88
      if (key == GLFW_KEY_1 && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
        mc.curr_node = mc.nodes.foot1;
      else if (key == GLFW_KEY_2 && action == GLFW_PRESS && mods & GLFW_MOD_SHIFT)
        mc.curr_node = mc.nodes.foot2;
      else if (key == GLFW_KEY_1 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes.body;
      else if (key == GLFW_KEY_2 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes.hand1;
      else if (key == GLFW_KEY_3 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes.hand2;
      else if (key == GLFW_KEY_4 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes.leg1;
      else if (key == GLFW_KEY_5 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes.leg2;
      else if (key == GLFW_KEY_6 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes.hand1lower;
      else if (key == GLFW_KEY_7 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes.hand2lower;
      else if (key == GLFW_KEY_8 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes.leg1lower;
      else if (key == GLFW_KEY_9 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes.leg2lower;
      else if (key == GLFW_KEY_0 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes.head2;
    } else { // R2-D2
      if (key == GLFW_KEY_1 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes2.body;
      else if (key == GLFW_KEY_2 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes2.head;
      else if (key == GLFW_KEY_3 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes2.plugarm;
      else if (key == GLFW_KEY_4 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes2.plug;
      else if (key == GLFW_KEY_5 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes2.leg1joint;
      else if (key == GLFW_KEY_6 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes2.leg2joint;
      else if (key == GLFW_KEY_7 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes2.foot1;
      else if (key == GLFW_KEY_8 && action == GLFW_PRESS)
        mc.curr_node = mc.nodes2.foot2;
    }
  }
};  
  


