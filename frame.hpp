#ifndef _FRAME_HPP_
#define _FRAME_HPP_

#include <fstream>
#include <iostream>
#include "hierarchy_node.hpp"

#define KEYFPS 1.0 // Number of keyframes per second
#define TWEENERS 10 // Number of tweener frames between keyframes

class frame;

struct Point {
    float x = 0, y = 0, z = 0;
    Point operator+(Point);
    Point operator*(float);
};

class BezierCurve {
    std::vector<Point> params;
    Point at(float t);

    public:
    std::vector<Point> points;
    BezierCurve(frame* f, int order);
};

class frame {

    public:
    
    ModifiableNodesParams nodes;
    ModifiableNodesR2Params nodes2;
    MovableParams camera, light1pos, light2pos, spotpos;
    int frameTime;
    int bezierOrder;
    BezierCurve * bz = NULL;
    int bezierIndex;
    
    frame() {}
    ~frame();
    frame(ModifiableNodesParams, ModifiableNodesR2Params, MovableParams, MovableParams, MovableParams, MovableParams);
    frame(std::string filename);
    void writeToFile();
    void interpolate(frame& start, frame& end, double t, int bt);
    void applyCameraPoint(Point p);
};

std::vector<frame> loadAllFrames(char* filename);

#endif