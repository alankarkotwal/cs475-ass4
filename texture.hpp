#ifndef _TEXTURE_HPP_
#define _TEXTURE_HPP_
#include <vector>

GLuint LoadTexture( const char * filename, int width, int height );
GLuint LoadSkyboxTexture( std::vector<std::string>, int width, int height );
void FreeTexture( GLuint texture );
#endif 
