#version 330

layout (location = 0) in vec3 position;
out vec3 texcoords;

uniform mat4 projview;

void main()
{
    gl_Position =   projview * vec4(position, 1.0);
    texcoords = position;
}