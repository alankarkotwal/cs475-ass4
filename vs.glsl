#version 330

in vec4 vPosition;
in vec4 vColor;
in vec4 vNormal;
in vec2 vTex;

out vec4 norm;
out vec4 pos;
out vec4 color;
out vec2 tex;
uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;

void main() {
    
    gl_Position = uViewMatrix * uModelMatrix * vPosition;
    pos = uModelMatrix * vPosition;
    norm = uModelMatrix * vNormal;
    tex = vTex;
}
