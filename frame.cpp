#include "frame.hpp"
#include <iostream>
#include <fstream>
#include <cmath>

// create a frame from contents
frame::frame(ModifiableNodesParams n, ModifiableNodesR2Params n2, MovableParams c, MovableParams l1p, MovableParams l2p, MovableParams sp) {
    
    nodes = n;
    nodes2 = n2;
    camera = c;
    light1pos = l1p;
    light2pos = l2p;
    spotpos = sp;
    frameTime = 0;
}

// read a frame from file
frame::frame(std::string filename) {
    std::ifstream file(filename.c_str());
    // Read IG-88 Stuff

    sscanf(filename.c_str(), "frame-%d.txt", &frameTime);

    nodes.body.read_from_file(file, false);

    nodes.hand1.read_from_file(file, false);

    nodes.hand2.read_from_file(file, false);

    nodes.leg1.read_from_file(file, false);

    nodes.leg2.read_from_file(file, false);

    nodes.hand1lower.read_from_file(file, false);

    nodes.hand2lower.read_from_file(file, false);

    nodes.leg1lower.read_from_file(file, false);

    nodes.leg2lower.read_from_file(file, false);

    nodes.head2.read_from_file(file, false);

    nodes.foot1.read_from_file(file, false);

    nodes.foot2.read_from_file(file, false);

    // Read R2-D2 Stuff

    nodes2.body.read_from_file(file, false);

    nodes2.head.read_from_file(file, false);

    nodes2.plugarm.read_from_file(file, false);

    nodes2.plug.read_from_file(file, false);

    nodes2.leg1joint.read_from_file(file, false);

    nodes2.leg2joint.read_from_file(file, false);

    nodes2.foot1.read_from_file(file, false);

    nodes2.foot2.read_from_file(file, false);


    // Movable stuff

    camera.read_from_file(file, false);

    light1pos.read_from_file(file, true);

    light2pos.read_from_file(file, true);

    spotpos.read_from_file(file, true);
}


frame::~frame() {

}

void frame::writeToFile() {
    
    std::cout << "Enter keyframe absolute time: ";
    std::cin >> frameTime;

    char filename[20];
    sprintf(filename, "frames/frame-%04d.txt", frameTime);
    std::ofstream ofs(filename, std::ofstream::app);
    
    //ofs << "# Frame" << frameTime << std::endl;
    //ofs << frameTime << std::endl;
    
    // Write IG-88 Stuff
    ofs << "## IG Body" << std::endl;
    nodes.body.print_to_file(ofs, false);
    ofs << "## IG Hand 1" << std::endl;
    nodes.hand1.print_to_file(ofs, false);
    ofs << "## IG Hand 2" << std::endl;
    nodes.hand2.print_to_file(ofs, false);
    ofs << "## IG Leg 1" << std::endl;
    nodes.leg1.print_to_file(ofs, false);
    ofs << "## IG Leg 2" << std::endl;
    nodes.leg2.print_to_file(ofs, false);
    ofs << "## IG Hand 1 lower" << std::endl;
    nodes.hand1lower.print_to_file(ofs, false);
    ofs << "## IG Hand 2 lower" << std::endl;
    nodes.hand2lower.print_to_file(ofs, false);
    ofs << "## IG Leg 1 lower" << std::endl;
    nodes.leg1lower.print_to_file(ofs, false);
    ofs << "## IG Leg 2 lower" << std::endl;
    nodes.leg2lower.print_to_file(ofs, false);
    ofs << "## IG Head" << std::endl;
    nodes.head2.print_to_file(ofs, false);
    ofs << "## IG Foot 1" << std::endl;
    nodes.foot1.print_to_file(ofs, false);
    ofs << "## IG Foot 2" << std::endl;
    nodes.foot2.print_to_file(ofs, false);

    // Write R2-D2 Stuff
    ofs << "## R2 Body" << std::endl;
    nodes2.body.print_to_file(ofs, false);
    ofs << "## R2 Head" << std::endl;
    nodes2.head.print_to_file(ofs, false);
    ofs << "## R2 Plug arm" << std::endl;
    nodes2.plugarm.print_to_file(ofs, false);
    ofs << "## R2 Plug" << std::endl;
    nodes2.plug.print_to_file(ofs, false);
    ofs << "## R2 Leg 1" << std::endl;
    nodes2.leg1joint.print_to_file(ofs, false);
    ofs << "## R2 leg 2" << std::endl;
    nodes2.leg2joint.print_to_file(ofs, false);
    ofs << "## R2 Foot 1" << std::endl;
    nodes2.foot1.print_to_file(ofs, false);
    ofs << "## R2 Foot 2" << std::endl;
    nodes2.foot2.print_to_file(ofs, false);


    // Movable stuff
    ofs << "## Camera" << std::endl;
    camera.print_to_file(ofs, false);
    ofs << "## Light 1" << std::endl;
    light1pos.print_to_file(ofs, true);
    ofs << "## Light 2" << std::endl;
    light2pos.print_to_file(ofs, true);
    ofs << "## Spotlight" << std::endl;
    spotpos.print_to_file(ofs, true);
    
    ofs.close();
    
    // frameNumber++;
}

// interpolate between two frames
// `t` is between 0 and 1
// `bt` is index in bezier curve
void frame::interpolate(frame& start, frame& end, double t, int bt) {
    nodes.interpolate(start.nodes, end.nodes, t);
    nodes2.interpolate(start.nodes2, end.nodes2, t);
    camera.interpolate(start.camera, end.camera, t);
    light1pos.interpolate(start.light1pos, end.light1pos, t);
    light2pos.interpolate(start.light2pos, end.light2pos, t);
    spotpos.interpolate(start.spotpos, end.spotpos, t);
    if(bz != NULL) {
        // std::cout<<bezierIndex*TWEENERS + bt<<" "<<bz->points.size()<<"\n";
        // std::cout<<camera.tx<<" "<<camera.ty<<" "<<camera.tz<<"\n";
        applyCameraPoint(bz->points[bezierIndex*TWEENERS + bt]);
    }

}

void frame::applyCameraPoint(Point p) {
    camera.tx = p.x;
    camera.ty = p.y;
    camera.tz = p.z;
}

// load a vector of frames from a keyframe list file
std::vector<frame> loadAllFrames(char* filename) {
    std::vector<frame> vec;
    std::ifstream keyframes(filename);
    while(!keyframes.eof()) {
        int theTime, bezier;
        char fname[30];
        keyframes>>theTime>>fname>>bezier;
        frame f(fname);
        f.frameTime = theTime;
        f.bezierOrder = bezier;
        vec.push_back(f);
    }

    for (int i=0;i<vec.size();i++) {
        if(vec[i].bezierOrder > 1) {
            BezierCurve* bz = new BezierCurve(&vec[i], vec[i].bezierOrder);
            for(int j = 0; j <= vec[i].bezierOrder; j++) {
                vec[i+j].bz = bz;
                vec[i+j].bezierIndex = j;
                vec[i+j].applyCameraPoint(bz->points[j*TWEENERS]);
            }
        }
    }
    return vec;
}

int choose(int n, int k) {
    if(2*k > n)
        k = n-k;
    if(k <= 0 || k >= n)
        return 1;

    int numerator = 1;
    int denominator = 1;
    for(int i = 1; i <= k; i++) {
        numerator *= (n - i + 1);
        denominator *= i;
    }
    return numerator / denominator;
}

// create a bezier curve from the camera position
// of this frame and subsequent frames
BezierCurve::BezierCurve(frame* f, int order) {
    params.clear();
    points.clear();
    if (order == 0)
        return;
    for(int k=0; k<=order; k++) {
        Point p;
        int ch = choose(order, k);
        p.x = ch*f[k].camera.tx;
        p.y = ch*f[k].camera.ty;
        p.z = ch*f[k].camera.tz;
        params.push_back(p);
    }

    int nkeyframes = f[order].frameTime - f[0].frameTime;
    for(int i=0; i<=nkeyframes*TWEENERS; i++) {
        points.push_back(at((i*1.0)/(nkeyframes*TWEENERS)));
    }
}

// point for Bezier parameter `t`
Point BezierCurve::at(float t) {
    Point p;
    int n = params.size();
    for(int k = 0; k < n; k++) {
        // std::cout<<t<<" "<<k<<" "<<std::pow(t, k)*std::pow(1-t, n-k-1)<<"\n";
        p = p + params[k]*std::pow(t, k)*std::pow(1-t, n-k);
    }
    return p;
}

Point Point::operator+(Point p) {
    Point r;
    r.x = x + p.x;
    r.y = y + p.y;
    r.z = z + p.z;
    return r;
}

Point Point::operator*(float f) {
    Point r;
    r.x = f*x;
    r.y = f*y;
    r.z = f*z;
    return r;
}
