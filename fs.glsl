#version 330
#define n_lights 2

in vec4 norm;
in vec4 pos;
in vec4 color;
in vec2 tex;

out vec4 frag_color;

uniform vec4 ambient, eye;
uniform vec4[n_lights] lights;
uniform vec4[n_lights] lightColors;
uniform int[n_lights] lightEnabled;
uniform vec4 spot;
uniform vec4 spotDir;
uniform vec4 spotColor;
uniform int spotEnabled;

uniform float shininess;
uniform sampler2D textureIn;

void main () {
    
    //vec4 texColor = texture(textureIn, vec2(fract(tex.x), fract(tex.y)));
    vec4 texColor = texture(textureIn, tex);
    vec4 spec = vec4(0.0, 0.0, 0.0, 1.0);
    float intensity = 0;
    
    for (int i=0; i<n_lights; i++) {
        if(lightEnabled[i] != 0) {
            float myDot = dot(normalize(vec3(lights[i] - pos)), normalize(vec3(norm)));
            intensity = intensity + max(myDot, 0);
            
            if (myDot > 0) {
                spec = spec + pow(max(dot(normalize(vec3(eye - pos) + vec3(lights[i] - pos)), normalize(vec3(norm))),0), shininess);
            }
        }
    }

    if (spotEnabled != 0) {
        float myDot = dot(normalize(vec3(spot - pos)), normalize(vec3(norm)));
        if (myDot > 0.1) {
            intensity = intensity + max(myDot, 0);
            spec = spec + pow(max(dot(normalize(vec3(eye - pos) + vec3(spot - pos)), normalize(vec3(norm))),0), shininess);
        }
    }
    
    frag_color = vec4(vec3(ambient)/2 + vec3(texColor*intensity)/2 + vec3(spec)/2, 1.0);
}