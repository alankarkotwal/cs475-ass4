CC=g++

ifeq ($(shell uname), Darwin)
	OPENGLLIB= -framework OpenGL
	GLEWLIB= -lglew
	GLFWLIB= -lglfw3
else
	OPENGLLIB= -lGL
	GLEWLIB= -lGLEW
	GLFWLIB = -lglfw
endif

LIBS=$(OPENGLLIB) $(GLEWLIB) $(GLFWLIB)
LDFLAGS=-L/usr/local/lib 
CPPFLAGS=-I/usr/local/include -I./ -Wno-write-strings -std=c++11
BIN=main
SRCS=main.cpp gl_framework.cpp shader_util.cpp hierarchy_node.cpp texture.cpp frame.cpp
INCLUDES=gl_framework.hpp shader_util.hpp main.hpp hierarchy_node.hpp texture.hpp frame.cpp

all: $(BIN)

$(BIN): $(SRCS) $(INCLUDES)
	g++ $(CPPFLAGS) $(SRCS) -o $(BIN) $(LDFLAGS) $(LIBS)

clean:
	rm -f *~ *.o $(BIN)
