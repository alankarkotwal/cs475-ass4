import os, sys
from math import sin,cos,pi,atan2
PI = 3.14159265

def cone(r1, r2, h, shards):
	arr = []
	nz = cos(atan2(h,(r2 -r1)))
	nx = sin(atan2(h,(r2 -r1)))
	if abs(nz) < 1e-10:
		nz = 0
	if abs(nx) < 1e-10:
		nx = 0
	for shard in range(0, shards):
		arr.append([
				((r1*cos(shard*2*pi/shards), r1*sin(shard*2*pi/shards), 0),
				 (nx*cos(shard*2*pi/shards), nx*sin(shard*2*pi/shards), nz),
				 (1.0*shard/shards, 0)
				),
				((r1*cos((shard+1)*2*pi/shards), r1*sin((shard + 1)*2*pi/shards), 0),
				 (nx*cos((shard+1)*2*pi/shards), nx*sin((shard + 1)*2*pi/shards), nz),
				 (1.0*(shard+1)/shards, 0),
				),
				((r2*cos(shard*2*pi/shards), r2*sin(shard*2*pi/shards), h),
				 (nx*cos(shard*2*pi/shards), nx*sin(shard*2*pi/shards), nz),
				 (1.0*shard/shards, 1)
				)
				])
		arr.append([
				((r2*cos(shard*2*pi/shards), r2*sin(shard*2*pi/shards), h),
				 (nx*cos(shard*2*pi/shards), nx*sin(shard*2*pi/shards), nz),
				 (1.0*shard/shards, 1),
				),
				((r1*cos((shard+1)*2*pi/shards), r1*sin((shard + 1)*2*pi/shards), 0),
				 (nx*cos((shard+1)*2*pi/shards), nx*sin((shard + 1)*2*pi/shards), nz),
				 (1.0*(shard+1)/shards, 0),
				),
				((r2*cos((shard+1)*2*pi/shards), r2*sin((shard+1)*2*pi/shards), h),
				 (nx*cos((shard+1)*2*pi/shards), nx*sin((shard + 1)*2*pi/shards), nz),
				 (1.0*(shard+1)/shards, 1),
				)
				])
		arr.append([
				((0,0,0),
				 (0,0,-1),
				 (0,0),
				),
				((r1*cos((shard+1)*2*pi/shards), r1*sin((shard + 1)*2*pi/shards), 0),
				 (0,0,-1),
				 (0.5+0.5*cos((shard+1)*2*pi/shards), 0.5+0.5*sin((shard + 1)*2*pi/shards)),
				),
				((r1*cos(shard*2*pi/shards), r1*sin(shard*2*pi/shards), 0),
				 (0,0,-1),
				 (0.5+0.5*cos(shard*2*pi/shards), 0.5+0.5*sin(shard*2*pi/shards)),
				),
				])
		arr.append([
				((0,0,h),
				 (0,0,1),
				 (0,0),
				),
				((r2*cos(shard*2*pi/shards), r2*sin(shard*2*pi/shards), h),
				 (0,0,1),
				 (0.5+0.5*cos(shard*2*pi/shards), 0.5+0.5*sin(shard*2*pi/shards)),
				),
				((r2*cos((shard+1)*2*pi/shards), r2*sin((shard + 1)*2*pi/shards), h),
				 (0,0,1),
				 (0.5+0.5*cos((shard+1)*2*pi/shards), 0.5+0.5*sin((shard + 1)*2*pi/shards)),
				),
				])
	return arr

def frust(r):
	return [
	[((-1,-1,0), (0.0, -2.0, -(r-1)), (0.5, 0.0)), ((1,-1,0), (0.0, -2.0, -(r-1)), (1.0, 1.0)), ((r,-r,2), (0.0, -2.0, -(r-1)), (0.0, 0.5))],
	[((-1,-1,0), (0.0, -2.0, -(r-1)), (0.5, 0.0)), ((r,-r,2), (0.0, -2.0, -(r-1)), (0.0, 0.5)), ((-r,-r,2), (0.0, -2.0, -(r-1)), (1.0, 1.0))],

	[((-1,-1,0), (-2.0, 0.0, -(r-1)), (1.0, 1.0)), ((-r,-r,2), (-2.0, 0.0, -(r-1)), (0.0, 0.5)), ((-1,1,0), (-2.0, 0.0, -(r-1)), (0.5, 0.0))],
	[((-r,-r,2), (-2.0, 0.0, -(r-1)), (0.0, 0.5)), ((-r,r,2), (-2.0, 0.0, -(r-1)), (0.0, 0.0)), ((-1,1,0), (-2.0, 0.0, -(r-1)), (0.5, 0.0))],

	[((-1,-1,0), (0.0, 0.0, -1.0), (0.0, 0.0)), ((-1,1,0), (0.0, 0.0, -1.0), (0.5, 0.0)), ((1,-1,0), (0.0, 0.0, -1.0), (0.0, 0.5))],
	[((-1,1,0), (0.0, 0.0, -1.0), (0.5, 0.0)), ((1,1,0), (0.0, 0.0, -1.0), (1.0, 1.0)), ((1,-1,0), (0.0, 0.0, -1.0), (0.0, 0.5))],

	[((1,1,0), (2.0, 0.0, -(r-1)), (0.0, 0.0)), ((r,r,2), (2.0, 0.0, -(r-1)), (0.0, 0.5)), ((1,-1,0), (2.0, 0.0, -(r-1)), (0.5, 0.0))],
	[((r,r,2), (2.0, 0.0, -(r-1)), (0.0, 0.5)), ((r,-r,2), (2.0, 0.0, -(r-1)), (1.0, 1.0)), ((1,-1,0), (2.0, 0.0, -(r-1)), (0.5, 0.0))],

	[((r,r,2), (0.0, 2.0, -(r-1)), (0.5, 0.0)), ((1,1,0), (0.0, 2.0, -(r-1)), (0.0, 0.0)), ((-1,1,0), (0.0, 2.0, -(r-1)), (0.0, 0.5))],
	[((r,r,2), (0.0, 2.0, -(r-1)), (0.5, 0.0)), ((-1,1,0), (0.0, 2.0, -(r-1)), (0.0, 0.5)), ((-r,r,2), (0.0, 2.0, -(r-1)), (1.0, 1.0))],

	[((r,r,2), (0.0, 0.0, 2.0), (0.0, 0.0)), ((-r,r,2), (0.0, 0.0, 2.0), (0.5, 0.0)), ((r,-r,2), (0.0, 0.0, 2.0), (0.0, 0.5))],
	[((-r,r,2), (0.0, 0.0, 2.0), (0.5, 0.0)), ((-r,-r,2), (0.0, 0.0, 2.0), (1.0, 1.0)), ((r,-r,2), (0.0, 0.0, 2.0), (0.0, 0.5))]
	]

def hemisphere(tess):
	arr = []
	slices = (180.0/(float(tess)*10.0))/2.0
	sectors = (180.0/(float(tess)*10.0))/2.0
	lats = 0
	while lats < PI/2.0:
		longs = 0
		while longs <= 2.0*PI:
			x = sin(lats) * cos(longs)
			y = sin(lats) * sin(longs)
			z = cos(lats)
			longs = longs + slices
			pt1 = (x, y, z)
			n1 = (x, y, z)
			latNew = lats+sectors
			longNew = longs+sectors
			if latNew>PI:
				latNew=PI;
			if longNew>PI:
				longNew=PI;
			x = sin(latNew) * cos(longs);
			y = sin(latNew) * sin(longs);
			z = cos(latNew);
			pt2 = (x, y, z)
			n2 = (x, y, z)
			x = sin(lats) * cos(longNew);
			y = sin(lats) * sin(longNew);
			z = cos(lats);
			pt3 = (x, y, z)
			n3 = (x, y, z)
			x = sin(latNew) * cos(longNew);
			y = sin(latNew) * sin(longNew);
			z = cos(latNew);
			pt4 = (x, y, z)
			n4 = (x, y, z)
			arr.append([(pt1, n1, (2.0*lats/PI, longs/(2.0*PI))), (pt2, n2, (2.0*latNew/PI, longs/(2.0*PI))), (pt3, n3, (2.0*lats/PI, longNew/(2.0*PI)))]);
			arr.append([(pt2, n2, (2.0*latNew/PI, longs/(2.0*PI))), (pt3, n3, (2.0*lats/PI, longNew/(2.0*PI))), (pt4, n4, (2.0*latNew/PI, longNew/(2.0*PI)))]);
		lats = lats + sectors
	return arr

def disp(tris):
	for tri in tris:
		for (vert, norm, tex) in tri:
			print vert[0],vert[1],vert[2], norm[0], norm[1], norm[2], tex[0], tex[1]#, 1, 0, 0

if sys.argv[1] == "cylinder":
	print(cone(float(sys.argv[2]), float(sys.argv[2]), float(sys.argv[3]), int(sys.argv[4])))

if sys.argv[1] == "cone":
	disp(cone(float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]), int(sys.argv[5])))

if sys.argv[1] == "frust":
	disp(frust(float(sys.argv[2])))

if sys.argv[1] == "cube":
	disp(frust(1))

if sys.argv[1] == "hsph":
	disp(hemisphere(sys.argv[2]))