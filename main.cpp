/*
CSX75 Tutorial 3

Use the arrow keys and PgUp,PgDn, 
keys to make the arms move.

Use the keys 1,2 and 3 to switch between arms.

Modified from An Introduction to OpenGL Programming, 
Ed Angel and Dave Shreiner, SIGGRAPH 2013

Written by - 
                         Harshavardhan Kode
*/


#include "main.hpp"
#include <glm/gtx/string_cast.hpp>

#define PRIM_FILE "primitives.txt"
#define SKBOX_RAD 5.0f

MainClass mc;

// see initModel() for an annotated example of usage
csX75::HNode* MainClass::makeNode(csX75::HNode* myParent, primType myType, glm::vec3 myTrans, float myXrot, float myYrot, float myZrot, glm::vec3 myScale, char* myTex, int myTexResX, int myTexResY, GLfloat myShininess, int movement_mask, MovableParams *params) {
    
    // Construct nodes
    std::vector<glm::vec4> thisElem, thisCols, thisNormals;
    std::vector<glm::vec2> thisTexs;
    glm::mat4 thisTransform;
    
    thisTransform = glm::translate(thisTransform, myTrans);
    thisTransform = glm::rotate(thisTransform, glm::radians(myXrot), glm::vec3(1.0f, 0.0f, 0.0f));
    thisTransform = glm::rotate(thisTransform, glm::radians(myYrot), glm::vec3(0.0f, 1.0f, 0.0f));
    thisTransform = glm::rotate(thisTransform, glm::radians(myZrot), glm::vec3(0.0f, 0.0f, 1.0f));
    thisTransform = glm::scale(thisTransform, myScale);
    
    for (int i=0; i<primitives[myType].size(); i++) {
            thisElem.push_back(thisTransform*primitives[myType][i]);
            thisCols.push_back(glm::vec4(1.0, 1.0, 1.0, 1.0));
            thisNormals.push_back(thisTransform*primNorms[myType][i]);
            thisTexs.push_back(primTexs[myType][i]);
    }
    
    tex = LoadTexture(myTex, myTexResX, myTexResY);
    
    csX75::HNode* myNode = new csX75::HNode(myParent, thisElem.size(), &thisElem[0], &thisCols[0], sizeof(glm::vec4)*thisElem.size(), sizeof(glm::vec4)*thisElem.size(), tex, &thisNormals[0], &thisTexs[0], sizeof(glm::vec4)*thisNormals.size(), sizeof(glm::vec2)*thisTexs.size(), myShininess, movement_mask, params);
    
    if(myParent == NULL) {
            root_list.push_back(myNode);
    }
    
    return myNode;
}

glm::vec3 homogenize(glm::vec4 v) {
   return glm::vec3(v.x/v.w,v.y/v.w,v.z/v.w);
}

void MainClass::initModel() {
    // Model construction
    // We make the model out of 3D "primitives". They are scaled along the scale vector, rotated along the angles specified, then shifted from the pivot point by the translation vector
    // We also supply texture information (file name, file size), shininess, and constraints
    nodes.body = makeNode(NULL,     PRIM_CYL,     glm::vec3(0.0, 0.0, 0.0), -90.0f, 0.0f, 0.0f, glm::vec3(3.0, 1.2, 5.0),"images/brushed.jpg", 1025, 768, 0.9f,        XROT|YROT|ZROT|XTRANS|YTRANS|ZTRANS,             &myFrame.nodes.body);
                          //parent  3D primitive  shift from pivot           rotation           scaling                   filename            file size   shininess    constraints (this one can move in all 6 DOFs)    associated MovableParams

    // we use change_parameters to change the initial position of the model. This does not change the pivot point.
    nodes.body->change_parameters(-4,0,-6,0,0.0,0);

    // Useful to scale the entire model, but this isn't really necessary for child nodes of a model
    nodes.body->scale(0.5,0.5,0.5);
    

    nodes.hand1 = makeNode(nodes.body, PRIM_CYL, glm::vec3(0,0,0), -90.0f, -90.0f, 0.0f, glm::vec3(0.8, 0.8, 0.6), "images/brushed.jpg", 1025, 768, 0.9f, XROT, &myFrame.nodes.hand1);
    nodes.hand1->change_parameters(-1.5, 4.4, 0.0,0.0,0.0,0.0);
    nodes.hand2 = makeNode(nodes.body, PRIM_CYL, glm::vec3(0,0,0), -90.0f, 90.0f, 0.0f, glm::vec3(0.8, 0.8, 0.6), "images/brushed.jpg", 1025, 768, 0.9f, XROT, &myFrame.nodes.hand2);
    nodes.hand2->change_parameters(1.5, 4.4, 0.0,0.0,0.0,0.0);
    nodes.hand1upper = makeNode(nodes.hand1, PRIM_CYL, glm::vec3(0,0,0), 90.0f, 0, 0, glm::vec3(0.3, 0.3, 3), "images/brushed.jpg", 1025, 768, 0.9f, FIXED, NULL);
    nodes.hand1upper->change_parameters(-0.3,0,0, 0,0,0);
    nodes.hand2upper = makeNode(nodes.hand2, PRIM_CYL, glm::vec3(0,0,0), 90.0f, 0, 0, glm::vec3(0.3, 0.3, 3), "images/brushed.jpg", 1025, 768, 0.9f, FIXED, NULL);
    nodes.hand2upper->change_parameters(0.3,0,0, 0,0,0);
    nodes.hand1lower = makeNode(nodes.hand1upper, PRIM_CYL, glm::vec3(0,0,0), 90.0f, 0.0f, 0, glm::vec3(0.3, 0.3, 3), "images/brushed.jpg", 1025, 768, 0.9f, XROT|YROT|ZROT, &myFrame.nodes.hand1lower);
    nodes.hand1lower->change_parameters(0,-3,0, -80,0,-30);
    nodes.hand2lower = makeNode(nodes.hand2upper, PRIM_CYL, glm::vec3(0,0,0), 90.0f, 0.0f, 0, glm::vec3(0.3, 0.3, 3), "images/brushed.jpg", 1025, 768, 0.9f, XROT|YROT|ZROT, &myFrame.nodes.hand2lower);
    nodes.hand2lower->change_parameters(0,-3,0, -110,0,10);
    nodes.leg1 = makeNode(nodes.body, PRIM_CYL, glm::vec3(0,0,0), -90.0f, 180.0f, 0.0f,  glm::vec3(1.6, 1.6, 0.3), "images/brushed.jpg", 1025, 768, 0.9f, YROT, &myFrame.nodes.leg1);
    nodes.leg1->change_parameters(-0.9, 0, 0.0,0.0,0.0,0.0);
    nodes.leg1upper = makeNode(nodes.leg1, PRIM_CYL, glm::vec3(0,0,0), -90.0f, 180.0f, 0.0f,  glm::vec3(1, 1, 2), "images/brushed.jpg", 1025, 768, 0.9f, FIXED, NULL);
    nodes.leg2 = makeNode(nodes.body, PRIM_CYL, glm::vec3(0.0, 0.0, 0.0), -90.0f, 180.0f, 0.0f, glm::vec3(1.6,1.6, 0.3), "images/brushed.jpg", 1025, 768, 0.9f, YROT, &myFrame.nodes.leg2);
    nodes.leg2->change_parameters(0.9, 0, 0.0,0.0,0.0,0.0);
    nodes.leg2upper = makeNode(nodes.leg2, PRIM_CYL, glm::vec3(0,0,0), -90.0f, 180.0f, 0.0f,  glm::vec3(1, 1, 2), "images/brushed.jpg", 1025, 768, 0.9f, FIXED, NULL);
    nodes.leg1lower = makeNode(nodes.leg1upper, PRIM_CYL, glm::vec3(0,0,0), 90.0f, 0.0f, 0, glm::vec3(0.3, 0.3, 4), "images/brushed.jpg", 1025, 768, 0.9f, XROT|YROT|ZROT, &myFrame.nodes.leg1lower);
    nodes.leg1lower->change_parameters(0,-2,0, 0,0,0);
    nodes.leg2lower = makeNode(nodes.leg2upper, PRIM_CYL, glm::vec3(0,0,0), 90.0f, 0.0f, 0, glm::vec3(0.3, 0.3, 4), "images/brushed.jpg", 1025, 768, 0.9f, XROT|YROT|ZROT, &myFrame.nodes.leg2lower);
    nodes.leg2lower->change_parameters(0,-2,0, 0,0,0);
    
    // feet are made of two parallel frustums with a flat rect prism below
    nodes.foot1 = makeNode(nodes.leg1lower, PRIM_CUBE, glm::vec3(0,-0.2,0), 90.0f, 0.0f, 0, glm::vec3(0.5,0.7, 0.1), "images/brushed.jpg", 1025, 768, 0.9f, XROT, &myFrame.nodes.foot1);
    nodes.foot1->change_parameters(0,-4,0, 0,0,0);
    nodes.foot1side1 = makeNode(nodes.foot1, PRIM_FRUST_1, glm::vec3(0,0,0), 90.0f,180,0, glm::vec3(0.2,0.7,0.4), "images/brushed.jpg", 1025, 768, 0.9f, FIXED, NULL);
    nodes.foot1side1->change_parameters(-0.3,-0.2,0,0,0,0);
    nodes.foot1side2 = makeNode(nodes.foot1, PRIM_FRUST_1, glm::vec3(0,0,0), 90.0f,180,0, glm::vec3(0.2,0.7,0.4), "images/brushed.jpg", 1025, 768, 0.9f, FIXED, NULL);
    nodes.foot1side2->change_parameters(0.3,-0.2,0,0,0,0);
    
    nodes.foot2 = makeNode(nodes.leg2lower, PRIM_CUBE, glm::vec3(0,-0.2,0), 90.0f, 0.0f, 0, glm::vec3(0.5,0.7, 0.1), "images/brushed.jpg", 1025, 768, 0.9f, XROT, &myFrame.nodes.foot2);
    nodes.foot2->change_parameters(0,-4,0, 0,0,0);
    nodes.foot2side1 = makeNode(nodes.foot2, PRIM_FRUST_1, glm::vec3(0,0,0), 90.0f,180,0, glm::vec3(0.2,0.7,0.4), "images/brushed.jpg", 1025, 768, 0.9f, FIXED, NULL);
    nodes.foot2side1->change_parameters(-0.3,-0.2,0,0,0,0);
    nodes.foot2side2 = makeNode(nodes.foot2, PRIM_FRUST_1, glm::vec3(0,0,0), 90.0f,180,0, glm::vec3(0.2,0.7,0.4), "images/brushed.jpg", 1025, 768, 0.9f, FIXED, NULL);
    nodes.foot2side2->change_parameters(0.3,-0.2,0,0,0,0);
    
    nodes.head1 = makeNode(nodes.body, PRIM_CFRUST_1, glm::vec3(0,0,0),90.0f,0,0, glm::vec3(0.4,0.4,0.5), "images/brushed.jpg", 1025, 768, 0.9f, FIXED, NULL);
    nodes.head1->change_parameters(0,5.5,0, 0,0,0);
    nodes.head2 = makeNode(nodes.head1, PRIM_CFRUST_2, glm::vec3(0,0.6,0),90.0f,0,0, glm::vec3(0.8,0.8,0.9), "images/brushed.jpg", 1025, 768, 0.9f, XROT|YROT|ZROT, &myFrame.nodes.head2);
    nodes.head2->change_parameters(0,0,0, 0,0,0);
    nodes.head3 = makeNode(nodes.head2, PRIM_CFRUST_1, glm::vec3(0,0.6,0),90.0f,0,0, glm::vec3(0.6,0.6,0.5), "images/brushed.jpg", 1025, 768, 0.9f, FIXED, NULL);
    nodes.head3->change_parameters(0,0.2,0, 0,0,0);
    nodes.head4 = makeNode(nodes.head2, PRIM_CYL, glm::vec3(0,0.6,0),90.0f,0,0, glm::vec3(1.2,1.2,0.5),  "images/brushed.jpg", 1025, 768, 0.9f, FIXED, NULL);
    nodes.head4->change_parameters(0,0.7,0, 0,0,0);
    nodes.head5 = makeNode(nodes.head2, PRIM_CFRUST_1, glm::vec3(0,0.6,0),90.0f,180,0, glm::vec3(0.6,0.6,0.5), "images/brushed.jpg", 1025, 768, 0.9f, FIXED, NULL);
    nodes.head5->change_parameters(0,0.7,0, 0,0,0);
    curr_node = nodes.body;


    // R2D2 starts here
    nodes2.body = makeNode(NULL, PRIM_CYL, glm::vec3(0.0, 0.0, 0.0), -90.0f, 0.0f, 90.0f, glm::vec3(2, 2, 2.0),"images/r2.bmp", 1000, 323, 0.5f, XROT|YROT|ZROT|XTRANS|YTRANS|ZTRANS, &myFrame.nodes2.body);
    nodes2.body->change_parameters(2,-3, -3,0,0.0,0);
    nodes2.body->scale(1,1,1);
    nodes2.head = makeNode(nodes2.body, PRIM_HRHSPH, glm::vec3(0.0, 2, 0.0), -90.0f, 0.0f,0.0f, glm::vec3(1,1,1),"images/r2.bmp", 1000, 323, 0.5f, YROT, &myFrame.nodes2.head);

    // the "arm" of the plug, can extend out of body
    nodes2.plugarm = makeNode(nodes2.body, PRIM_CYL, glm::vec3(0.0, 0, 0.0), 0,0,0,glm::vec3(0.2,0.2,1), "images/brushed.jpg", 1025, 768, 0.9f, ZTRANS, &myFrame.nodes2.plugarm);
    nodes2.plugarm->change_parameters(0,1,-0.15,0,0.0,0);

    // the plug itself
    nodes2.plug = makeNode(nodes2.plugarm, PRIM_CYL, glm::vec3(0.0, 0, 0.0), 0,0,0,glm::vec3(0.3,0.3,0.1), "images/brushed.jpg", 1025, 768, 0.9f, ZROT, &myFrame.nodes2.plug);
    nodes2.plug->change_parameters(0,0,1,0,0.0,0);

    nodes2.leg1joint=makeNode(nodes2.body, PRIM_CYL, glm::vec3(0.0, 0, 0.0), -90,90,0,glm::vec3(1,1,0.4),"images/white.bmp", 256, 265, 1.0f, XROT, &myFrame.nodes2.leg1joint);
    nodes2.leg1joint->change_parameters(-1.3,1.7,0,0,0.0,0);
    nodes2.leg2joint=makeNode(nodes2.body, PRIM_CYL, glm::vec3(0.0, 0, 0.0), -90,-90,0,glm::vec3(1,1,0.4),"images/white.bmp", 256, 265, 1.0f, XROT, &myFrame.nodes2.leg2joint);
    nodes2.leg2joint->change_parameters(1.3,1.7,0,0,0.0,0);

    nodes2.leg1=makeNode(nodes2.leg1joint, PRIM_CUBE, glm::vec3(0.0, 0, 0.0), 0,0,0,glm::vec3(0.15,1,0.5),"images/white.bmp", 256, 265, 1.0f, FIXED, NULL);
    nodes2.leg1->change_parameters(0.15,-1,-0.5,0,0.0,0);
    nodes2.foot1=makeNode(nodes2.leg1, PRIM_FRUST_1, glm::vec3(0, -0.5, 0.0), 90.0f,180,0,glm::vec3(0.4,0.5,0.5),"images/white.bmp", 256, 265, 1.0f, XROT, &myFrame.nodes2.foot1);
    nodes2.foot1->change_parameters(0,-0.8,0.5,0,0.0,0);

    nodes2.leg2=makeNode(nodes2.leg2joint, PRIM_CUBE, glm::vec3(0.0, 0, 0.0), 0,0,0,glm::vec3(0.15,1,0.5),"images/white.bmp", 256, 265, 1.0f, FIXED, NULL);
    nodes2.leg2->change_parameters(-0.15,-1,-0.5,0,0.0,0);
    nodes2.foot2=makeNode(nodes2.leg2, PRIM_FRUST_1, glm::vec3(0, -0.5, 0.0), 90.0f,180,0,glm::vec3(0.4,0.5,0.5),"images/white.bmp", 256, 265, 1.0f, XROT, &myFrame.nodes2.foot2);
    nodes2.foot2->change_parameters(0,-0.8,0.5,0,0.0,0);

    csX75::HNode* ground = makeNode(NULL, PRIM_SQ, glm::vec3(0, -4,0), 90,0,0, glm::vec3(29,29 ,1), "images/sand2.bmp", 2048,2048, 0.5f, FIXED, NULL);

    // test saving and loading
    // frame f("frames/frame-0001.txt");
    // myFrame = f;
    // nodes.update_from_params(f.nodes);
    // nodes2.update_from_params(f.nodes2);
}

void MainClass::initBuffersGL(void)
{
    // initialize openGL state


    // Load shaders and use the resulting shader program
    std::string vertex_shader_file("vs.glsl");
    std::string fragment_shader_file("fs.glsl");

    std::vector<GLuint> shaderList;
    shaderList.push_back(csX75::LoadShaderGL(GL_VERTEX_SHADER, vertex_shader_file));
    shaderList.push_back(csX75::LoadShaderGL(GL_FRAGMENT_SHADER, fragment_shader_file));

    shaderProgram = csX75::CreateProgramGL(shaderList);
    
    std::string vertex_shader_skbox_file("vs_skbox.glsl");
    std::string fragment_shader_skbox_file("fs_skbox.glsl");
    
    shaderList.clear();
    shaderList.push_back(csX75::LoadShaderGL(GL_VERTEX_SHADER, vertex_shader_skbox_file));
    shaderList.push_back(csX75::LoadShaderGL(GL_FRAGMENT_SHADER, fragment_shader_skbox_file));
    
    shaderProgramSkbox = csX75::CreateProgramGL(shaderList);

    // getting the attributes from the shader program
    vPosition = glGetAttribLocation( shaderProgram, "vPosition" );
    vColor = glGetAttribLocation( shaderProgram, "vColor" ); 
    uModelMatrix = glGetUniformLocation( shaderProgram, "uModelMatrix");
    uViewMatrix = glGetUniformLocation( shaderProgram, "uViewMatrix");
    vNorm = glGetAttribLocation( shaderProgram, "vNormal" );
    vTex = glGetAttribLocation( shaderProgram, "vTex" );
    eye = glGetUniformLocation( shaderProgram, "eye" );
    ambComp = glGetUniformLocation( shaderProgram, "ambient" );
    lights = glGetUniformLocation( shaderProgram, "lights" );
    lightColors = glGetUniformLocation( shaderProgram, "lightColors" );
    lightEnabled = glGetUniformLocation( shaderProgram, "lightEnabled" );
    spot = glGetUniformLocation( shaderProgram, "spot" );
    spotDir = glGetUniformLocation( shaderProgram, "spotDir" );
    spotColor = glGetUniformLocation( shaderProgram, "spotColor" );
    spotEnabled = glGetUniformLocation( shaderProgram, "spotEnabled" );
    shiny = glGetUniformLocation( shaderProgram, "shininess" );

    vSkboxPos = glGetAttribLocation( shaderProgramSkbox, "position" );
    vSkboxProjView = glGetUniformLocation( shaderProgramSkbox, "projview" );
    
    std::vector<std::string> skyboxNames;
    skyboxNames.push_back("images/skright.bmp");
    skyboxNames.push_back("images/skleft.bmp");
    skyboxNames.push_back("images/sktop.bmp");
    skyboxNames.push_back("images/skbottom.bmp");
    skyboxNames.push_back("images/skback.bmp");
    skyboxNames.push_back("images/skfront.bmp");
    
    cubeTex = LoadSkyboxTexture(skyboxNames, 512, 500);

    // Creating the hierarchy:
    std::ifstream prims(PRIM_FILE, std::ifstream::in);
    
    std::string line;
    std::getline(prims, line);
    
    for (int i=0; i<=PRIM_NONE; i++) {
            
            // Parse primitive i
            std::vector<glm::vec4> thisPrim, thisPrimCol, thisPrimNorms;
            std::vector<glm::vec2> thisPrimTexs;
            
            while(std::getline(prims, line)) {
                    if(!line.empty()) {
                        // If line starts with #, this primitive has ended.
                        if(line[0] == '#') {
                                break;
                        }
                        
                        // Else parse line to push next vertex.
                        std::istringstream iss(line);
                        
                        float temp[8];
                        int j=0;
                        
                        while(!iss.eof()) {
                                iss >> temp[j];
                                j++;
                        }
                        
                        thisPrim.push_back(glm::vec4(temp[0], temp[1], temp[2], 1.0));
                        thisPrimCol.push_back(glm::vec4(1.0, 1.0, 1.0, 1.0));
                        thisPrimNorms.push_back(glm::vec4(temp[3], temp[4], temp[5], 1.0));
                        thisPrimTexs.push_back(glm::vec2(temp[6], temp[7]));                            
                    }
            }
            
            primitives.push_back(thisPrim);
            primCols.push_back(thisPrimCol);
            primNorms.push_back(thisPrimNorms);
            primTexs.push_back(thisPrimTexs);
    }

    // Construct nodes
    std::vector<glm::vec4> thisElem, thisCols, thisNormals;
    std::vector<glm::vec2> thisTexs;
    glm::mat4 thisTransform;

    initModel();

    myFrame.light1pos.enabled = true;
    myFrame.light2pos.enabled = true;
    myFrame.spotpos.enabled = true;
    myFrame.spotpos.rx=-190.0;
    myFrame.spotpos.ry=-180.0;
    
    float skyboxPoints[] = {
            -SKBOX_RAD,    SKBOX_RAD, -SKBOX_RAD,
            -SKBOX_RAD, -SKBOX_RAD, -SKBOX_RAD,
            SKBOX_RAD, -SKBOX_RAD, -SKBOX_RAD,
            SKBOX_RAD, -SKBOX_RAD, -SKBOX_RAD,
            SKBOX_RAD,    SKBOX_RAD, -SKBOX_RAD,
            -SKBOX_RAD,    SKBOX_RAD, -SKBOX_RAD,
            
            -SKBOX_RAD, -SKBOX_RAD,    SKBOX_RAD,
            -SKBOX_RAD, -SKBOX_RAD, -SKBOX_RAD,
            -SKBOX_RAD,    SKBOX_RAD, -SKBOX_RAD,
            -SKBOX_RAD,    SKBOX_RAD, -SKBOX_RAD,
            -SKBOX_RAD,    SKBOX_RAD,    SKBOX_RAD,
            -SKBOX_RAD, -SKBOX_RAD,    SKBOX_RAD,
            
            SKBOX_RAD, -SKBOX_RAD, -SKBOX_RAD,
            SKBOX_RAD, -SKBOX_RAD,    SKBOX_RAD,
            SKBOX_RAD,    SKBOX_RAD,    SKBOX_RAD,
            SKBOX_RAD,    SKBOX_RAD,    SKBOX_RAD,
            SKBOX_RAD,    SKBOX_RAD, -SKBOX_RAD,
            SKBOX_RAD, -SKBOX_RAD, -SKBOX_RAD,
            
            -SKBOX_RAD, -SKBOX_RAD,    SKBOX_RAD,
            -SKBOX_RAD,    SKBOX_RAD,    SKBOX_RAD,
            SKBOX_RAD,    SKBOX_RAD,    SKBOX_RAD,
            SKBOX_RAD,    SKBOX_RAD,    SKBOX_RAD,
            SKBOX_RAD, -SKBOX_RAD,    SKBOX_RAD,
            -SKBOX_RAD, -SKBOX_RAD,    SKBOX_RAD,
            
            -SKBOX_RAD,    SKBOX_RAD, -SKBOX_RAD,
            SKBOX_RAD,    SKBOX_RAD, -SKBOX_RAD,
            SKBOX_RAD,    SKBOX_RAD,    SKBOX_RAD,
            SKBOX_RAD,    SKBOX_RAD,    SKBOX_RAD,
            -SKBOX_RAD,    SKBOX_RAD,    SKBOX_RAD,
            -SKBOX_RAD,    SKBOX_RAD, -SKBOX_RAD,
            
            -SKBOX_RAD, -SKBOX_RAD, -SKBOX_RAD,
            -SKBOX_RAD, -SKBOX_RAD,    SKBOX_RAD,
            SKBOX_RAD, -SKBOX_RAD, -SKBOX_RAD,
            SKBOX_RAD, -SKBOX_RAD, -SKBOX_RAD,
            -SKBOX_RAD, -SKBOX_RAD,    SKBOX_RAD,
            SKBOX_RAD, -SKBOX_RAD,    SKBOX_RAD
    };
    
    //Ask GL for a Vertex Attribute Objects (vao)
    glGenVertexArrays (1, &skyboxVAO);
    //Ask GL for aVertex Buffer Object (vbo)
    glGenBuffers (1, &skyboxVBO);
    
    //bind them
    glBindVertexArray (skyboxVAO);
    glBindBuffer (GL_ARRAY_BUFFER, skyboxVBO);
    
    glBufferData ( GL_ARRAY_BUFFER, sizeof(skyboxPoints), skyboxPoints, GL_STATIC_DRAW );
    
    //setup the vertex array as per the shader
    glEnableVertexAttribArray( vSkboxPos );
    glVertexAttribPointer( vSkboxPos, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );


}

void MainClass::renderGL(void)
{
    // render current frame to the screen


    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    matrixStack.clear();

    // Set up my unifoms
    glUniform4fv(ambComp, 1, glm::value_ptr(glm::vec4(0.2, 0.2, 0.2, 1.0)));

    glm::vec4 myLights[2];
    glm::mat4 light1_mat = myFrame.light1pos.as_matrix();
    glm::mat4 light2_mat = myFrame.light2pos.as_matrix();
    glm::mat4 spot_mat = myFrame.spotpos.as_matrix();
    myLights[0] = light1_mat*glm::vec4(2, -4, -4, 1.0);
    myLights[1] = light2_mat*glm::vec4(0, 0, -15, 1.0);
    glm::vec4 mySpot = spot_mat*glm::vec4(0,4,-4, 1.0);
    glm::vec4 mySpotDir = spot_mat*glm::vec4(0, -1, 1, 1.0);
    glUniform4fv(lights, 2, glm::value_ptr(myLights[0]));
    glUniform4fv(spot, 1, glm::value_ptr(mySpot));
    glUniform4fv(spotDir, 1, glm::value_ptr(mySpotDir));

    glm::vec4 myLightCols[2];
    myLightCols[0] = glm::vec4(1.0, 1.0, 1.0, 1.0);
    myLightCols[1] = glm::vec4(1.0, 1.0, 1.0, 1.0);
    glm::vec4 mySpotColor  = glm::vec4(1.0, 1.0, 1.0, 1.0);
    glUniform4fv(lightColors, 2, glm::value_ptr(myLightCols[0]));
    glUniform4fv(spotColor,1, glm::value_ptr(mySpotColor));

    int myLightEnabled[2] = {myFrame.light1pos.enabled+0, myFrame.light2pos.enabled+0};
    glUniform1iv(lightEnabled, 2, &myLightEnabled[0]);
    glUniform1i(spotEnabled, myFrame.spotpos.enabled+0);

    //Creating the lookat and the up vectors for the camera
    c_rotation_matrix = glm::rotate(glm::mat4(1.0f), glm::radians(c_xrot), glm::vec3(1.0f,0.0f,0.0f));
    c_rotation_matrix = glm::rotate(c_rotation_matrix, glm::radians(c_yrot), glm::vec3(0.0f,1.0f,0.0f));
    c_rotation_matrix = glm::rotate(c_rotation_matrix, glm::radians(c_zrot), glm::vec3(0.0f,0.0f,1.0f));

    glm::mat4 camera_mat = myFrame.camera.as_matrix();
    glm::vec4 c_pos = camera_mat*glm::vec4(c_xpos,c_ypos,c_zpos, 1.0)*c_rotation_matrix;
    glm::vec4 c_up = camera_mat*glm::vec4(c_up_x,c_up_y,c_up_z, 1.0)*c_rotation_matrix;
    //Creating the lookat matrix
    lookat_matrix = glm::lookAt(homogenize(c_pos), homogenize(camera_mat*glm::vec4(0.0, 0.0, 0.0, 1)),homogenize(c_up));

    glUniform4fv(eye, 1, glm::value_ptr(c_pos));

    //creating the projection matrix
    if(enable_perspective)
        projection_matrix = glm::frustum(-1.0, 1.0, -1.0, 1.0, 1.0, 14.0);
        //projection_matrix = glm::perspective(glm::radians(90.0),1.0,0.1,5.0);
    else
        projection_matrix = glm::ortho(-7.0, 7.0, -7.0, 7.0, -5.0, 5.0);

    view_matrix = projection_matrix*lookat_matrix;
        
    //matrixStack.push_back(view_matrix);
    
    // Render the skybox
    glDepthMask(GL_FALSE);
    glUseProgram( shaderProgramSkbox );
    glUniformMatrix4fv(vSkboxProjView, 1, GL_FALSE, glm::value_ptr(view_matrix));
    
    glBindVertexArray (skyboxVAO);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTex);
    glDrawArrays (GL_TRIANGLES, 0, 36);
    glDepthMask(GL_TRUE);
    
    // Render the scene
    glUseProgram( shaderProgram );
    glUniformMatrix4fv(uViewMatrix, 1, GL_FALSE, glm::value_ptr(view_matrix));

    for (int i=0; i<root_list.size(); i++) {
            root_list[i]->render_tree();
    }

}

void screendump(int W, int H, int number) {
    char fn[30];
    sprintf(fn, "render/frame-%04d.tga", number);
    FILE   *out = fopen(fn,"wb");
    char   *pixel_data = new char[3*W*H];
    short  TGAhead[] = { 0, 2, 0, 0, 0, 0, W, H, 24 };
     
    glReadBuffer(GL_BACK);
    glReadPixels(0, 0, W, H, GL_BGR, GL_UNSIGNED_BYTE, pixel_data);
     
    fwrite(&TGAhead,sizeof(TGAhead),1,out);
    fwrite(pixel_data, 3*W*H, 1, out);
    fclose(out);
     
    delete[] pixel_data;
}
int MainClass::main(int argc, char** argv)
{
    //! The pointer to the GLFW window
    GLFWwindow* window;

    //! Setting up the GLFW Error callback
    glfwSetErrorCallback(csX75::error_callback);

    //! Initialize GLFW
    if (!glfwInit())
        return -1;

    //We want OpenGL 4.0
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); 
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    //This is for MacOSX - can be omitted otherwise
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); 
    //We don't want the old OpenGL 
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); 
    int winsize = 1024;
    //! Create a windowed mode window and its OpenGL context
    window = glfwCreateWindow(winsize,winsize, "Our Awesome Star Wars Scene", NULL, NULL);
    if (!window)
        {
            glfwTerminate();
            return -1;
        }
    
    //! Make the window's context current 
    glfwMakeContextCurrent(window);

    //Initialize GLEW
    //Turn this on to get Shader based OpenGL
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (GLEW_OK != err)
        {
            //Problem: glewInit failed, something is seriously wrong.
            std::cerr<<"GLEW Init Failed : %s"<<std::endl;
        }

    //Keyboard Callback
    glfwSetKeyCallback(window, csX75::key_callback);
    //Framebuffer resize callback
    glfwSetFramebufferSizeCallback(window, csX75::framebuffer_size_callback);

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    //Initialize GL state
    csX75::initGL();
    initBuffersGL();
    bool animate = false;
    bool render = false;
    int number = 0;
    if (argc > 1) {
        if(strcmp(argv[1], "animate") == 0) {
            animate = true;
        }
        if(strcmp(argv[1], "load") == 0) {
            myFrame = frame(argv[2]);            
            nodes.update_from_params(myFrame.nodes);
            nodes2.update_from_params(myFrame.nodes2);
        }
        if(strcmp(argv[1], "render") == 0) {
            animate = true;
            render = true;
        }
    }

    int currentFrameNo = 0;
    int currentKeyFrameNo = 0;
    double stamp = 0;
    double lastFrameStamp = 0;
    int stride = 0;
    int bt = 0;
    if(animate) {
        // load keyframes
        char* filename = "keyframes.txt";
        if(argc > 2) {
            filename = argv[2];
        }
        frames = loadAllFrames(filename);
        myFrame = frames[0];
        renderGL();
        if(render)
            screendump(winsize,winsize, 0);
        stride = frames[1].frameTime - frames[0].frameTime;
        glfwSetTime(0);   
    }

    double spf = 1/(KEYFPS*TWEENERS);
    double spkf = 1/KEYFPS;

    // Loop until the user closes the window
    while (glfwWindowShouldClose(window) == 0)
        {
            if(animate) {
                double newStamp = glfwGetTime();
                if (render) {
                    newStamp = stamp + 1.01*spf;
                    number++;
                }

                if((newStamp - stamp) > (spf)) {
                    stamp = newStamp;
                    if(stamp - lastFrameStamp < stride*spkf) {
                        bt++;
                        myFrame.interpolate(frames[currentFrameNo], frames[currentFrameNo+1], (stamp - lastFrameStamp)/(spkf*stride), bt);
                    } else if (currentFrameNo < frames.size() - 2) {
                        currentFrameNo++;
                        myFrame = frames[currentFrameNo];
                        lastFrameStamp = stamp;
                        stride = frames[currentFrameNo+1].frameTime - frames[currentFrameNo].frameTime;
                        bt = 0;
                    } else {
                        animate = false;

                    }


                    nodes.update_from_params(myFrame.nodes);
                    nodes2.update_from_params(myFrame.nodes2);
                }
            }
            
            // Render here
            renderGL();
            if(render) {
            screendump(winsize,winsize, number);   
            }

            // Swap front and back buffers
            glfwSwapBuffers(window);
            // Poll for and process events
            glfwPollEvents();
        }
    
    glfwTerminate();
    return 0;
}

int main(int argc, char** argv) {
    mc.main(argc, argv);
}

//-------------------------------------------------------------------------

