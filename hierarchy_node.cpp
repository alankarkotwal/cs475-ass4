#include "hierarchy_node.hpp"

#include "main.hpp"

#include <iostream>

extern MainClass mc;

namespace csX75
{

	HNode::HNode(HNode* a_parent, GLuint num_v, glm::vec4* a_vertices, glm::vec4* a_colours, std::size_t v_size, std::size_t c_size, GLuint _myTex, glm::vec4* a_norms, glm::vec2* a_texs, std::size_t n_size, std::size_t t_size, GLfloat _shininess, int mask, MovableParams* frameParams){

		num_vertices = num_v;
		vertex_buffer_size = v_size;
		color_buffer_size = c_size;
        norm_buffer_size = n_size;
        tex_buffer_size = t_size;
        shininess = _shininess;
		// initialize vao and vbo of the object;


		//Ask GL for a Vertex Attribute Objects (vao)
		glGenVertexArrays (1, &vao);
		//Ask GL for aVertex Buffer Object (vbo)
		glGenBuffers (1, &vbo);

		//bind them
		glBindVertexArray (vao);
		glBindBuffer (GL_ARRAY_BUFFER, vbo);
        
        // Get texture
        myTex = _myTex;
		
		glBufferData (GL_ARRAY_BUFFER, vertex_buffer_size + color_buffer_size + norm_buffer_size + tex_buffer_size, NULL, GL_STATIC_DRAW);
		glBufferSubData( GL_ARRAY_BUFFER, 0, vertex_buffer_size, a_vertices );
		glBufferSubData( GL_ARRAY_BUFFER, vertex_buffer_size, color_buffer_size, a_colours );
        glBufferSubData( GL_ARRAY_BUFFER, vertex_buffer_size + color_buffer_size, norm_buffer_size, a_norms );
        glBufferSubData( GL_ARRAY_BUFFER, vertex_buffer_size + color_buffer_size + norm_buffer_size, tex_buffer_size, a_texs );

		//setup the vertex array as per the shader
		glEnableVertexAttribArray( mc.vPosition );
		glVertexAttribPointer( mc.vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );

		glEnableVertexAttribArray( mc.vColor );
		glVertexAttribPointer( mc.vColor, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertex_buffer_size) );

        glEnableVertexAttribArray( mc.vNorm );
        glVertexAttribPointer( mc.vNorm, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertex_buffer_size + color_buffer_size) );
        
        glEnableVertexAttribArray( mc.vTex );
        glVertexAttribPointer( mc.vTex, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertex_buffer_size + color_buffer_size + norm_buffer_size) );

		// set parent

		if(a_parent == NULL){
			parent = NULL;
		}
		else{
			parent = a_parent;
			parent->add_child(this);
		}
		movement_mask = mask;
		//initial parameters are set to 0;

		tx=ty=tz=rx=ry=rz=0;
		sx=sy=sz=1;
		params = frameParams;
		update_matrices();
	}

	void HNode::update_matrices(){

		rotation = glm::rotate(glm::mat4(1.0f), glm::radians(rx), glm::vec3(1.0f,0.0f,0.0f));
		rotation = glm::rotate(rotation, glm::radians(ry), glm::vec3(0.0f,1.0f,0.0f));
		rotation = glm::rotate(rotation, glm::radians(rz), glm::vec3(0.0f,0.0f,1.0f));

		translation = glm::translate(glm::mat4(1.0f),glm::vec3(tx,ty,tz));
		scaling = glm::scale(glm::mat4(1.0f), glm::vec3(sx,sy,sz));

		if (params != NULL) {
			params->tx = tx;
			params->ty = ty;
			params->tz = tz;
			params->rx = rx;
			params->ry = ry;
			params->rz = rz;
		}

	}

	void HNode::add_child(HNode* a_child){
		children.push_back(a_child);

	}

	void HNode::change_parameters(GLfloat atx, GLfloat aty, GLfloat atz, GLfloat arx, GLfloat ary, GLfloat arz){
		tx = atx;
		ty = aty;
		tz = atz;
		rx = arx;
		ry = ary;
		rz = arz;

		update_matrices();
	}

	// Mostly should only be used by root nodes for easy resizing
	void HNode::scale(GLfloat asx, GLfloat asy, GLfloat asz){
		sx = asx; 
		sy = asy;
		sz = asz;

		update_matrices();
	}

	void HNode::render(){

		//mc.matrixStack multiply
		glm::mat4* ms_mult = multiply_stack(mc.matrixStack);
		glUniformMatrix4fv(mc.uModelMatrix, 1, GL_FALSE, glm::value_ptr(*ms_mult));
        glUniform1f(mc.shiny, shininess);
		glBindVertexArray (vao);
        glBindTexture(GL_TEXTURE_2D, myTex);
		glDrawArrays(GL_TRIANGLES, 0, num_vertices);

		// for memory 
		delete ms_mult;

	}

	void HNode::render_tree(){
		
		mc.matrixStack.push_back(scaling);
		mc.matrixStack.push_back(translation);
		mc.matrixStack.push_back(rotation);

		render();
		for(int i=0;i<children.size();i++){
			children[i]->render_tree();
		}
		mc.matrixStack.pop_back();
		mc.matrixStack.pop_back();
		mc.matrixStack.pop_back();

	}

	void HNode::inc_rx(){
		if(movement_mask & XROT) {
			rx+=10;
			update_matrices();
		}
	}


	void HNode::inc_ry(){
		if(movement_mask & YROT) {
			ry+=10;
			update_matrices();
		}
	}

	void HNode::inc_rz(){
		if(movement_mask & ZROT) {
			rz+=10;
			update_matrices();
		}
	}

	void HNode::dec_rx(){
		if(movement_mask & XROT) {
			rx-=10;
			update_matrices();
		}
	}

	void HNode::dec_ry(){
		if(movement_mask & YROT) {
			ry-=10;
			update_matrices();
		}
	}

	void HNode::dec_rz(){
		if(movement_mask & ZROT) {
			rz-=10;
			update_matrices();
		}
	}

	void HNode::inc_tx(){
		if(movement_mask & XTRANS) {
			tx+=0.2;
			update_matrices();
		}
	}


	void HNode::inc_ty(){
		if(movement_mask & YTRANS) {
			ty+=0.2;
			update_matrices();
		}
	}

	void HNode::inc_tz(){
		if(movement_mask & ZTRANS) {
			tz+=0.2;
			update_matrices();
		}
	}

	void HNode::dec_tx(){
		if(movement_mask & XTRANS) {
			tx-=0.2;
			update_matrices();
		}
	}

	void HNode::dec_ty(){
		if(movement_mask & YTRANS) {
			ty-=0.2;
			update_matrices();
		}
	}

	void HNode::dec_tz(){
		if(movement_mask & ZTRANS) {
			tz-=0.2;
			update_matrices();
		}
	}

	void HNode::update_from_params(MovableParams& mp) {
		change_parameters(mp.tx, mp.ty, mp.tz, mp.rx, mp.ry, mp.rz);
	}

	glm::mat4* multiply_stack(std::vector<glm::mat4> matStack){
		glm::mat4* mult;
		mult = new glm::mat4(1.0f);
	
		for(int i=0;i<matStack.size();i++){
			*mult = (*mult) * matStack[i];
		}	

		return mult;
	}

}

glm::mat4 MovableParams::as_matrix() {
		glm::mat4 mat = glm::rotate(glm::mat4(1.0f), glm::radians(rx), glm::vec3(1.0f,0.0f,0.0f));
		mat = glm::rotate(mat, glm::radians(ry), glm::vec3(0.0f,1.0f,0.0f));
		mat = glm::rotate(mat, glm::radians(rz), glm::vec3(0.0f,0.0f,1.0f));

		mat = glm::translate(mat,glm::vec3(tx,ty,tz));
		return mat;
}

void MovableParams::read_from_file(std::ifstream& s, bool light) {
	char c[256];
	while(true) {
		s.getline(c, 256);
		if(c[0] == '#' || c[0] == '\0') {
			continue;
		}
		
		if(light) {
			int i_enabled;
			sscanf(c, "%f\t%f\t%f\t%f\t%f\t%f\t%d", &tx, &ty, &tz, &rx, &ry, &rz, &i_enabled);
			enabled = (bool)i_enabled;
		} else {
			sscanf(c, "%f\t%f\t%f\t%f\t%f\t%f", &tx, &ty, &tz, &rx, &ry, &rz);
		}
		return;
	}
}

void MovableParams::print_to_file(std::ofstream& s, bool light) {

		s<<tx<< "\t"<<ty<< "\t"<<tz<< "\t"<<rx<< "\t"<<ry<< "\t"<<rz<< "\t";
		if(light) {
			s<<enabled;
		}
		s<<std::endl;
}

void ModifiableNodes::update_from_params(ModifiableNodesParams& mp) {
	body->update_from_params(mp.body);
	hand1->update_from_params(mp.hand1);
	hand2->update_from_params(mp.hand2);
	leg1->update_from_params(mp.leg1);
	leg2->update_from_params(mp.leg2);
	hand1lower->update_from_params(mp.hand1lower);
	hand2lower->update_from_params(mp.hand2lower);
	leg1lower->update_from_params(mp.leg1lower);
	leg2lower->update_from_params(mp.leg2lower);
	head2->update_from_params(mp.head2);
	foot1->update_from_params(mp.foot1);
	foot2->update_from_params(mp.foot2);
}
void ModifiableNodesR2::update_from_params(ModifiableNodesR2Params& mp) {
	body->update_from_params(mp.body);
	head->update_from_params(mp.head);
	leg1joint->update_from_params(mp.leg1joint);
	leg2joint->update_from_params(mp.leg2joint);
	foot1->update_from_params(mp.foot1);
	foot2->update_from_params(mp.foot2);
	plugarm->update_from_params(mp.plugarm);
	plug->update_from_params(mp.plug);
}

void MovableParams::interpolate(MovableParams& start, MovableParams& end, double t) {
	tx = (end.tx - start.tx)*t + start.tx;
	ty = (end.ty - start.ty)*t + start.ty;
	tz = (end.tz - start.tz)*t + start.tz;
	rx = (end.rx - start.rx)*t + start.rx;
	ry = (end.ry - start.ry)*t + start.ry;
	rz = (end.rz - start.rz)*t + start.rz;
}

void ModifiableNodesParams::interpolate(ModifiableNodesParams& start, ModifiableNodesParams& end, double t) {
	body.interpolate(start.body, end.body, t);
	hand1.interpolate(start.hand1, end.hand1, t);
	hand2.interpolate(start.hand2, end.hand2, t);
	leg1.interpolate(start.leg1, end.leg1, t);
	leg2.interpolate(start.leg2, end.leg2, t);
	hand1lower.interpolate(start.hand1lower, end.hand1lower, t);
	hand2lower.interpolate(start.hand2lower, end.hand2lower, t);
	leg1lower.interpolate(start.leg1lower, end.leg1lower, t);
	leg2lower.interpolate(start.leg2lower, end.leg2lower, t);
	head2.interpolate(start.head2, end.head2, t);
	foot1.interpolate(start.foot1, end.foot1, t);
	foot2.interpolate(start.foot2, end.foot2, t);
}

void ModifiableNodesR2Params::interpolate(ModifiableNodesR2Params& start, ModifiableNodesR2Params& end, double t) {
	body.interpolate(start.body, end.body, t);
	head.interpolate(start.head, end.head, t);
	leg1joint.interpolate(start.leg1joint, end.leg1joint, t);
	leg2joint.interpolate(start.leg2joint, end.leg2joint, t);
	foot1.interpolate(start.foot1, end.foot1, t);
	foot2.interpolate(start.foot2, end.foot2, t);
	plugarm.interpolate(start.plugarm, end.plugarm, t);
	plug.interpolate(start.plug, end.plug, t);

}